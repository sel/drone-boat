# Raspberry Pi Boat Drone

![][IMG_20160521_195915]

### Overview

This is a simple project to build a boat drone robot using a Raspberry Pi. This drone is one of the simplest you can build and uses two DC motors to control a pair of propellers.

There is a simple interface to send directional control to the drone and also receive a live camera stream back over the internet.

### Parts and Tools

You will need the following parts and tools to build this drone. At the time of writing (May 2016) the total cost is roughly £160.

- [Raspberry Pi 3]
- [SD Card]
- [WiFi Dongle]
- [Battery Pack] -- To power the Pi.
- [Adafruit DC & Stepper Motor HAT] -- This has everything needed to drive the motors from the Pi.
- [Brass M2.5 Standoffs] -- These help hold the Motor HAT securely to the Pi.
- [Camera Module] -- Optionally, purchase one with a mount to help secure the camera to the boat.
- [RC Boat] -- Ideally, any cheap RC boat with two DC motors and a rechargeable battery pack which is big enough to house the Pi.
- [Jumper Wires]
- [Precision Screwdrivers], [Soldering Tools] and optionally to mount the camera correctly a drill, coping saw and glue gun.

### Assembly

Start the project by following [Adafruit's motor HAT guide] to assemble and test the HAT.

Next, open up the RC boat, desolder the motors and remove the RC components not required to power the boat.

![][IMG_20160521_184613]

The boat used here doesn't have enough room to house the Pi out of the box. Therefore, some additional modifications were made to cut away some of the internal plastics.

![][IMG_20160521_191336]

Cut a hole in the boat in a suitable location to mount the camera. The photo shows how a drill was used to create a hole in to which a coping saw blade could be used to cut a strip for the camera ribbon to be inserted. A glue gun can be used to set the camera in place and prevent water from entering.

![][IMG_20160521_192444]

Finally, using jumper wires and screwdrivers where necessary, hook up the motors and power to the HAT, and the camera, wifi and power to the Pi. Keep access to the Pi open for now so we can easily reach the SD card and connections as we work on the software.

![][IMG_20160521_195829]

### Software

[Download and install the latest version of Raspbian] to the SD card and [use raspi-config] to expand the file system and enable the camera.

Next, [enable wifi on the Pi]. Then follow [Adafruit's motor HAT guide software installation], including [enabling I2C on the Pi].

Then, we install the RPi Cam Web Interface from its GitHub repo. 

```
git clone https://github.com/silvanmelchior/RPi_Cam_Web_Interface.git
cd RPi_Cam_Web_Interface
chmod u+x RPi_Cam_Web_Interface_Installer.sh
./RPi_Cam_Web_Interface_Installer.sh install
```

Follow the instructions on-screen to install via Apache.

The idea is to develop a web server using a Python microframework called [Flask] which can be connected to to control the motors.

To install Flask, you'll need to have pip installed. Run the following command to install pip, Apache and mod_wsgi which will be used to run the web server.

```
sudo apt-get install python-pip apache2 libapache2-mod-wsgi
```

Then,  use pip to install Flask and its dependencies.

```
sudo pip install flask
```

Pull down a copy of this repo to your /home/pi directory.

```
git clone git@bitbucket.org:sel/drone-boat.git
```

Then, copy over the Apache default VirtualHost settings and restart Apache.

```
sudo cp /home/pi/drone-boat/000-default.conf /etc/apache2/sites-enabled/
sudo apache2ctl restart
```

Now you can visit your Pi from a touch-enabled device on the same network by entering your Pi's IP address in to the browser (e.g. 192.168.1.XXX).

[Raspberry Pi 3]: <https://shop.pimoroni.com/collections/raspberry-pi/products/raspberry-pi-3>
[SD Card]: <https://shop.pimoroni.com/products/noobs-8gb-sd-card>
[WiFi Dongle]: <https://shop.pimoroni.com/products/official-raspberry-pi-wifi-dongle>
[Battery Pack]: <https://www.amazon.co.uk/Anker-Portable-External-Technology-Motorola/dp/B005NFOA0S/>
[Adafruit DC & Stepper Motor HAT]: <https://shop.pimoroni.com/products/adafruit-dc-stepper-motor-hat-for-raspberry-pi-mini-kit>
[Brass M2.5 Standoffs]: <https://shop.pimoroni.com/products/brass-m2-5-standoffs-for-pi-hats-black-plated-pack-of-2>
[Camera Module]: <https://shop.pimoroni.com/products/raspberry-pi-camera-module-v2-1-with-mount>
[RC Boat]: <https://www.amazon.co.uk/gp/product/B00G7XJJBU>
[Jumper Wires]: <https://shop.pimoroni.com/products/jumper-lead-selection>
[Precision Screwdrivers]: <https://www.amazon.co.uk/Flying-Colourz-Jewellerys-Precision-Screwdriver/dp/B01DADNBLI>
[Soldering Tools]: <https://www.amazon.co.uk/Ex-Pro®-Complete-Soldering-Iron-240v/dp/B005UD712M>

[Adafruit's motor HAT guide]: <https://learn.adafruit.com/adafruit-dc-and-stepper-motor-hat-for-raspberry-pi/overview>
[Download and install the latest version of Raspbian]: <https://www.raspberrypi.org/downloads/raspbian/>
[use raspi-config]: <https://www.raspberrypi.org/documentation/configuration/raspi-config.md>
[enable wifi on the Pi]: <http://www.howtogeek.com/167425/how-to-setup-wi-fi-on-your-raspberry-pi-via-the-command-line/>
[Adafruit's motor HAT guide software installation]: <https://learn.adafruit.com/adafruit-dc-and-stepper-motor-hat-for-raspberry-pi/installing-software>
[enabling I2C on the Pi]: <https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c>
[Flask]: <http://flask.pocoo.org/>

[IMG_20160521_183849]: <https://bytebucket.org/sel/drone-boat/raw/fb000c1a1dd480bbddbc8120a65878dacc2ae9c0/photos/IMG_20160521_183849.jpg>
[IMG_20160521_184613]: <https://bytebucket.org/sel/drone-boat/raw/fb000c1a1dd480bbddbc8120a65878dacc2ae9c0/photos/IMG_20160521_184613.jpg>
[IMG_20160521_191336]: <https://bytebucket.org/sel/drone-boat/raw/fb000c1a1dd480bbddbc8120a65878dacc2ae9c0/photos/IMG_20160521_191336.jpg>
[IMG_20160521_192444]: <https://bytebucket.org/sel/drone-boat/raw/fb000c1a1dd480bbddbc8120a65878dacc2ae9c0/photos/IMG_20160521_192444.jpg>
[IMG_20160521_195829]: <https://bytebucket.org/sel/drone-boat/raw/fb000c1a1dd480bbddbc8120a65878dacc2ae9c0/photos/IMG_20160521_195829.jpg>
[IMG_20160521_195915]: <https://bytebucket.org/sel/drone-boat/raw/fb000c1a1dd480bbddbc8120a65878dacc2ae9c0/photos/IMG_20160521_195915.jpg>

