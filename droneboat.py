from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor
from flask import Flask, render_template, request, send_from_directory
import time
import atexit

# create a default object, no changes to I2C address or frequency
mh = Adafruit_MotorHAT(addr=0x60)

# recommended for auto-disabling motors on shutdown!
def turnOffMotors():
    mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)

atexit.register(turnOffMotors)

app = Flask(__name__, static_url_path='')

@app.route("/")
def main():
    templateData = {
    }

    return render_template('main.html', **templateData)

@app.route("/<left>/<right>")
def speed(left, right):
    leftMotor = mh.getMotor(1)
    rightMotor = mh.getMotor(2)
    
    leftMotorSpeed = int(left)
    rightMotorSpeed = int(right)

    if leftMotorSpeed > 0:
        leftMotor.run(Adafruit_MotorHAT.FORWARD)
        leftMotor.setSpeed(abs(leftMotorSpeed))
    elif leftMotorSpeed < 0:
        leftMotor.run(Adafruit_MotorHAT.BACKWARD)
        leftMotor.setSpeed(abs(leftMotorSpeed))
    else:
        leftMotor.run(Adafruit_MotorHAT.RELEASE)

    if rightMotorSpeed > 0:
        rightMotor.run(Adafruit_MotorHAT.FORWARD)
        rightMotor.setSpeed(abs(rightMotorSpeed))
    elif rightMotorSpeed < 0:
        rightMotor.run(Adafruit_MotorHAT.BACKWARD)
        rightMotor.setSpeed(abs(rightMotorSpeed))
    else:
        rightMotor.run(Adafruit_MotorHAT.RELEASE)

    templateData = {
    }

    return render_template('main.html', **templateData)

#@app.route('/js/<path:path>')
#def send_js(path):
#    return send_from_directory('js', path)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)

