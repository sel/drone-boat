var radius = 250,

    joystick = new VirtualJoystick({
        limitStickTravel: true,
        stickRadius: radius
    }),

    request = new XMLHttpRequest(),
    ping = function() {
        var coordinates = circleCoordinatesToSquareCoordinates({
                x: joystick.deltaX() * (1 / radius),
                y: joystick.deltaY() * (1 / radius)
            }),

            x = coordinates.x,
            y = coordinates.y,

            // Compute the angle in deg
            z = Math.sqrt(x * x + y * y),     // First hypotenuse
            rad = Math.acos(Math.abs(x) / z), // angle in radians
            angle = rad * 180 / Math.PI,      // and in degrees

            // Now angle indicates the measure of turn
            // Along a straight line, with an angle o, the turn co-efficient is same
            // this applies for angles between 0-90, with angle 0 the co-eff is -1
            // with angle 45, the co-efficient is 0 and with angle 90, it is 1
            tcoeff = -1 + (angle / 90) * 2,
            turn = tcoeff * Math.abs(Math.abs(y) - Math.abs(x)),

            // And max of y or x is the movement
            move = Math.max(Math.abs(y), Math.abs(x));

        turn = Math.round(turn * 100) / 100;

        // First and third quadrant
        if((x >= 0 && y >= 0) || (x < 0 &&  y < 0)) {
            left = move;
            right = turn;
        } else {
            right = move;
            left = turn;
        }

        // Reverse polarity
        if(y < 0) {
            left = 0 - left;
            right = 0 - right;
        }

        if(isNaN(left)) {
            left = 0;
        }

        if(isNaN(right)) {
            right = 0;
        }

        request.open('GET', Math.floor(left * radius) + '/' + Math.floor(right * radius), true);
        request.send();
    },

    circleCoordinatesToSquareCoordinates = function(circleCoordinates) {
        var x = circleCoordinates.x,
            y = circleCoordinates.y,

            squareCoordinates = {};

        // ½ √( 2 + u² - v² + 2u√2 ) - ½ √( 2 + u² - v² - 2u√2 )
        squareCoordinates.x = (Math.sqrt(2 + (x * x) - (y * y) + (2 * x * Math.sqrt(2))) / 2) - (Math.sqrt(2 + (x * x) - (y * y) - (2 * x * Math.sqrt(2))) / 2);

        // ½ √( 2 - u² + v² + 2v√2 ) - ½ √( 2 - u² + v² - 2v√2 )
        squareCoordinates.y = (Math.sqrt(2 - (x * x) + (y * y) + (2 * y * Math.sqrt(2))) / 2) - (Math.sqrt(2 - (x * x) + (y * y) - (2 * y * Math.sqrt(2))) / 2);

        return squareCoordinates;
    };

joystick.addEventListener('touchStartValidation', function(event) {
    request.onload = ping;
    ping();
});

joystick.addEventListener('touchEnd', function(event) {
    request.onload = function() {};
    request.open('GET', '0/0', true);
    request.send();
});
