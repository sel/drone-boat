var mjpeg_img,
    video_fps = 25,
    preview_delay = 0,
    ajax_status = new XMLHttpRequest();

document.addEventListener('DOMContentLoaded', function() {
    mjpeg_img = document.getElementById('mjpeg_dest');
    preview_delay = Math.floor(1 / video_fps * 1000000);

    ajax_status.open('GET', '/camera/cmd_pipe.php?cmd=ro%200', true);
    ajax_status.send();

    ajax_status.open('GET', '/camera/status_mjpeg.php', true);
    ajax_status.onreadystatechange = function() {
        mjpeg_img.src = '/camera/cam_pic_new.php?time=' + new Date().getTime() + '&pDelay=' + preview_delay;
    };
    ajax_status.send();
});

